//
//  Response.swift
//  CoreProject
//
//  Created by Iurii Poluektov on 29.03.2018.
//
//

import Foundation
import Alamofire

public enum Response<T> {
    case success(T)
    case error(Error)
}
