//
//  Operation.swift
//  CoreProject
//
//  Created by Iurii Poluektov on 29.03.2018.
//
//


import Foundation
import RxSwift
import ObjectMapper

protocol Operation {
    associatedtype Output
    associatedtype ResponseType
    var request: RequestModel? {get}
    func execute(in dispatcher: NetworkDispatcher) -> Observable<Output>
}






