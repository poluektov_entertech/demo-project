//
//  NetworkDispatcher.swift
//  CoreProject
//
//  Created by Iurii Poluektov on 29.03.2018.
//
//
//

import Foundation
import Alamofire
import ObjectMapper

public protocol NetworkDispatcher {
    
    init(environment: NetworkEnvironment)
    
    func execute<T:Mappable>(request: RequestModel, completionHandler: @escaping (Response<T>) -> Void) -> DataRequest
    
}
