//
//  RequestModel.swift
//  CoreProject
//
//  Created by Iurii Poluektov on 29.03.2018.
//
//

import Foundation
import Alamofire

public protocol RequestModel {
    var path : String {get}
    var method : HTTPMethod {get}
    var parameters : RequestParams {get}
    var headers : [String : String]? {get}
}

public enum RequestParams {
    case body(_ : [String: Any]?, encoding: ParameterEncoding)
    case url(_ : [String: Any]?, encoding : ParameterEncoding)
}

public enum DataType {
    case JSON
    case Data
}

extension RequestModel {
    
    public var headers : [String : String]? {
        return nil
    }
    
    public func asURLRequest(environment: NetworkEnvironment)  -> URLRequest? {
        
        let fullURL = try? "\(environment.host)/\(self.path)".asURL()
        
        guard let URL = fullURL else {
            assertionFailure("Is not valid URL!")
            return nil
        }
        
        var request = URLRequest(url: URL)
        request.httpMethod = self.method.rawValue
        
        do {
            request = try self.prepareParameters().parameterEncoding.encode(request, with: self.prepareParameters().params)
        } catch {
            assertionFailure("Encoding parameters throw error!")
        }
        
        for (key, value) in environment.headers {
            request.addValue(value, forHTTPHeaderField: key)
        }
        
        if let requestHeaders = self.headers {
            for (key, value) in requestHeaders {
                request.addValue(value, forHTTPHeaderField: key)
            }
        }
        return request
    }

    
    private func prepareParameters () -> (params: Parameters?, parameterEncoding : ParameterEncoding) {
        switch self.parameters {
        case let .url(params, encoding):
            return (params, encoding)
        case let .body(params, encoding):
            return (params, encoding)
        }
    }
    
}
