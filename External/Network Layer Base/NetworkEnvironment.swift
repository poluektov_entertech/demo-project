//
//  NetworkEnvironment.swift
//  CoreProject
//
//  Created by Iurii Poluektov on 29.03.2018.
//
//

import Foundation

public protocol NetworkEnvironment {
    var name : String {get}
    var host : String {get}
    var headers: [String: String] {get}
    var cachePolicy: URLRequest.CachePolicy {get}
}
