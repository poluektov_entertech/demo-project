//
//  UIViewStyle.swift
//  CoreProject
//
//  Created by Iurii Poluektov on 29.03.2018.
//
//  Struct for style implementation in UIView. It's a base for style system in project
//

import UIKit

struct UIViewStyle<T: UIView> {
    
    let styling: (T) -> Void
    
    static func compose(_ styles: UIViewStyle<T>...) -> UIViewStyle<T> {
        return UIViewStyle { view in
            styles.forEach { style in style.styling(view)}
        }
    }
    
    func composing(with other: UIViewStyle<T>) -> UIViewStyle<T> {
        return UIViewStyle { view in
            self.styling(view)
            other.styling(view)
        }
    }
    
    func composing(with otherStyling:  @escaping (T) -> Void) -> UIViewStyle<T> {
        return self.composing(with: UIViewStyle(styling: otherStyling))
    }
    
    func apply(to view: T) {
        styling(view)
    }
    
    func apply(to views: T...) {
        views.forEach { view in
            styling(view)
        }
    }
}
