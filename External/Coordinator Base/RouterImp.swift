
import Foundation
import UIKit

final class RouterImp: Router {
    
    //MARK:
    
    private var rootController: UINavigationController?
    private var completions: [UIViewController : () -> Void] = [:]
    
    init(rootController: UINavigationController) {
        self.rootController = rootController
    }
    
    //MARK: - Router
    
    func present(_ module: Presentable?) {
        self.present(module, animated: true, presentCompletion: nil)
    }
    
    func present(_ module: Presentable?, animated: Bool) {
        self.present(module, animated: animated, presentCompletion: nil)
    }
    
    func present(_ module: Presentable?, animated: Bool = true, presentCompletion:(() -> Void)? = nil) {
        guard let controller = module?.toPresent() else { return }
        
        rootController?.present(controller, animated: animated, completion: presentCompletion)
    }
    
    func dismissModule() {
        self.dismissModule(animated: true, completion: nil)
    }
    
    func dismissModule(animated: Bool = true, completion: (() -> Void)? = nil) {
        rootController?.dismiss(animated: animated, completion: completion)
    }
    
    func push(_ module: Presentable?) {
        self.push(module, animated: true, completion: nil)
    }
    
    func push(_ module: Presentable?, animated: Bool) {
        self.push(module, animated: animated, completion: nil)
    }
    
    func push(_ module: Presentable?, animated: Bool, completion: (() -> Void)?) {
        guard let controller = module?.toPresent(), (controller is UINavigationController == false)
            else { assertionFailure("Deprecated push UINavigationController.");
            return
        }
        
        if let completion = completion {
            completions[controller] = completion
        }
        rootController?.pushViewController(controller, animated: animated)
    }
    
    func popModule() {
        self.popModule(animated: true)
    }
    
    func popModule(animated: Bool)  {
        if let controller = rootController?.popViewController(animated: animated) {
            runCompletion(for: controller)
        }
    }
    
    func setRootModule(_ module: Presentable?) {
        self.setRootModule(module, hideBar: false)
    }

    func setRootModule(_ module: Presentable?, hideBar: Bool) {
        guard let controller = module?.toPresent() else {
            return
        }
        rootController?.setViewControllers([controller], animated: false)
        rootController?.isNavigationBarHidden = hideBar
        
    }
    
    func popToRootModule(animated: Bool) {
        if let controllers = rootController?.popToRootViewController(animated: animated) {
            controllers.forEach { controller in
                runCompletion(for: controller)
            }
        }
    }
    
    //MARK: - Presentable 
    
    func toPresent() -> UIViewController? {
        return rootController
    }
    
    //MARK: - Private
    
    private func runCompletion(for controller: UIViewController) {
        guard let completion = completions[controller] else { return }
        completion()
        completions.removeValue(forKey: controller)
    }
}
