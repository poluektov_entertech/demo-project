
import Foundation

class BaseCoordinator: Coordinator {
    
    typealias Option  = [String:Any]
    
    var childCoordinators: [Coordinator] = []
    
    var completionHandler: (() -> ())?
    
    func start() {
        assertionFailure("Need ovveride start from BaseCoordinator")
    }
    
    func start(with option: LinkCommand? = nil) {
        assertionFailure("Need ovveride start from BaseCoordinator")
    }
    
    func addDependency(_ coordinator: Coordinator) {
        for element in childCoordinators {
            if element === coordinator { return }
        }
        childCoordinators.append(coordinator)
    }
    
    func removeDependency(_ coordinator: Coordinator?) {
        guard
            childCoordinators.isEmpty == false,
            let coordinator = coordinator
            else { return }
        
        for (index, element) in childCoordinators.enumerated() {
            if element === coordinator {
                childCoordinators.remove(at: index)
                break
            }
        }
    }
}
