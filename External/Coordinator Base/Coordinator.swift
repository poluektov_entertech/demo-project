
protocol Coordinator: class {
    
    func start()
    
    func start(with option: LinkCommand?)
    
    var completionHandler: (()->())? {get set}
    
}
