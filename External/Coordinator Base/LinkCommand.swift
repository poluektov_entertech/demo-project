
import Foundation

protocol LinkCommand {
    
    func execute()
    
}

