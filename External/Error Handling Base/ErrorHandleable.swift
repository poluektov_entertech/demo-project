
import Foundation

public typealias HandleAction<T> = (T) throws -> ()

public protocol ErrorHandleable: class {
    // Func for throw error with finally block
    func `throw`(_: Error, finally: @escaping (Bool) -> Void)
    // Func for catching Error
    func `catch`(action: @escaping HandleAction<Error>) -> ErrorHandleable
}
